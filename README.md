# README #

This is a set of scratch files for the Midnight Curry OWC 2015 team.  If you are reading this, you got to the source code repository!

### What is this repository for? ###

* Quick summary
An attempt to get some housekeeping/setup work done before we start cranking away.


### How do I get set up? ###

copy these files locally into a folder called "owcurry" 
(that stands for 
          Overnight
          Web
          Curry)

### Contribution guidelines ###

none

### Who do I talk to? ###

* whomever you make up your mind to.

The more I learn, the more I realize how much I don't know.
- Albert Einstein

In theory, theory and practice are the same. In practice, they are not.
- Albert Einstein